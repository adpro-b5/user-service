package com.charitable.user.controller;

import com.charitable.user.core.RegisterRequest;
import com.charitable.user.exception.PasswordMismatchesException;
import com.charitable.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class RegisterController {
    @Autowired
    private UserService userService;

    @PostMapping(value = "/register", produces = {"application/json"})
    public ResponseEntity createUser(@Valid @RequestBody RegisterRequest request) {
        if (!request.getPassword().equals(request.getPasswordConfirm())) {
            throw new PasswordMismatchesException("Password confirmation did not match.");
        }
        userService.createUser(request);
        return ResponseEntity.noContent().build();
    }
}
